terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  alias  = "us_east_1"
  region = "us-east-1"
}

provider "aws" {
  region = "eu-west-2"
}

terraform {
  backend "s3" {
    bucket = "zaktfbucket"
    key    = "terraform/key"
    region = "eu-west-2"
  }
}

# STATIC SITE RESOURCES
//the s3 bucket itself
resource "aws_s3_bucket" "static-site-bucket" {
  bucket = "zakcvsite"
}

//lifting public access blocks for static site bucket
resource "aws_s3_bucket_public_access_block" "static-site-bucket" {
  bucket = aws_s3_bucket.static-site-bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

//configuring static site website to access index and error html files, not neccessary with cloudfront
# resource "aws_s3_bucket_website_configuration" "static-site-bucket" {
#   bucket = aws_s3_bucket.static-site-bucket.id

#   index_document {
#     suffix = "index.html"
#   }

#   error_document {
#     key = "error.html"
#   }
# }

//telling static site bucket to direct logs to the log bucket
resource "aws_s3_bucket_logging" "static-site-bucket" {
  bucket = aws_s3_bucket.static-site-bucket.id

  target_bucket = aws_s3_bucket.log-bucket.id
  target_prefix = "log/"
}

//static site bucket policy allowing public access
resource "aws_s3_bucket_policy" "static-website-policy" {
  bucket = aws_s3_bucket.static-site-bucket.id

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": {
        "Sid": "AllowCloudFrontServicePrincipalReadOnly",
        "Effect": "Allow",
        "Principal": {
            "Service": "cloudfront.amazonaws.com"
        },
        "Action": "s3:GetObject",
        "Resource": "${aws_s3_bucket.static-site-bucket.arn}/*",
        "Condition": {
            "StringEquals": {
                "AWS:SourceArn": [
                  "${aws_cloudfront_distribution.zakcvsite-cloudfront.arn}",
                  "${aws_cloudfront_distribution.zakcvsite-blog-cloudfront.arn}"
                ]
            }
        }
    }
}
EOF
}

# LOG BUCKET RESOURCES
//creating the bucket
resource "aws_s3_bucket" "log-bucket" {
  bucket = "zakcvsite-logs"
}

//enabling ACL for log bucket
resource "aws_s3_bucket_ownership_controls" "log-bucket" {
  bucket = aws_s3_bucket.log-bucket.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

//setting ACL to log configuration
resource "aws_s3_bucket_acl" "log-bucket-acl" {
  bucket = aws_s3_bucket.log-bucket.id
  acl    = "log-delivery-write"
}

//configuring log bucket to delete any files older than 2 days
resource "aws_s3_bucket_lifecycle_configuration" "log-bucket-config" {
  bucket = aws_s3_bucket.log-bucket.id
  rule {
    id = "delete after 2 days"
    expiration {
      days = 2
    }
    status = "Enabled"
  }
}

# CLOUDFRONT RESOURCES

resource "aws_cloudfront_distribution" "zakcvsite-cloudfront" {
  origin {
    //s3 bucket regional domain
    domain_name = aws_s3_bucket.static-site-bucket.bucket_regional_domain_name
    origin_id   = "zakcvsite-origin"
    //connect to OAC
    origin_access_control_id = aws_cloudfront_origin_access_control.zakcvsite-oac.id
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "CloudFront distribution for zakcvsite"
  default_root_object = "index.html"

  aliases = ["zakmarden.com", "www.zakmarden.com"]

  default_cache_behavior {
    cache_policy_id  = "658327ea-f89d-4fab-a63d-7e88639e58f6"
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "zakcvsite-origin"

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations        = ["GB"]
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.cert.arn
    ssl_support_method = "sni-only"
  }
}

//BLOG cloudfront resource

resource "aws_cloudfront_distribution" "zakcvsite-blog-cloudfront" {
  
  origin {
    //s3 bucket regional domain
    domain_name = aws_s3_bucket.static-site-bucket.bucket_regional_domain_name
    origin_id   = "zakcvsite-blog-origin"
    //connect to OAC
    origin_access_control_id = aws_cloudfront_origin_access_control.zakcvsite-oac.id
    origin_path = "/blog"
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "CloudFront distribution for zakcvsite blog"
  default_root_object = "index.html"

  aliases = ["blog.zakmarden.com"]

  default_cache_behavior {
    cache_policy_id  = "658327ea-f89d-4fab-a63d-7e88639e58f6"
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "zakcvsite-blog-origin"

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations        = ["GB"]
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.cert.arn
    ssl_support_method = "sni-only"
  }
}

resource "aws_cloudfront_origin_access_control" "zakcvsite-oac" {
  name = "zakcvsite-oac"
  origin_access_control_origin_type = "s3"
  signing_behavior                  = "always"
  signing_protocol                  = "sigv4"
}

# AWS Certificate Manager Resources

resource "aws_acm_certificate" "cert" {
  provider = aws.us_east_1
  domain_name       = "zakmarden.com"
  subject_alternative_names = ["*.zakmarden.com"]
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

# Route 53 Config

import {
  to = aws_route53_zone.cv-zone
  id = var.hosted_zone
}

resource "aws_route53_zone" "cv-zone" {
  name = "zakmarden.com"
}

resource "aws_route53_record" "root" {
  zone_id = aws_route53_zone.cv-zone.zone_id
  name    = "zakmarden.com"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.zakcvsite-cloudfront.domain_name
    zone_id                = aws_cloudfront_distribution.zakcvsite-cloudfront.hosted_zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.cv-zone.zone_id
  name    = "www.zakmarden.com"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.zakcvsite-cloudfront.domain_name
    zone_id                = aws_cloudfront_distribution.zakcvsite-cloudfront.hosted_zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "blog" {
  zone_id = aws_route53_zone.cv-zone.zone_id
  name    = "blog.zakmarden.com"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.zakcvsite-blog-cloudfront.domain_name
    zone_id                = aws_cloudfront_distribution.zakcvsite-blog-cloudfront.hosted_zone_id
    evaluate_target_health = true
  }
}